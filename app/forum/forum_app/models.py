from django.db import models

class User(models.Model):
	name = models.CharField(max_length=30, unique=True)
	location = models.CharField(max_length=128, blank=True)
	registered = models.DateField('date user registered', null=True)

	def __unicode__(self):
		return self.name

	def __eq__(self, other):
		return (isinstance(other, self.__class__) and self.name == other.name)


class Topic(models.Model):
	create_user = models.ForeignKey(User)
	subject = models.CharField(max_length=128)
	create_date = models.DateTimeField()
	
	def __unicode__(self):
		return self.subject

	def __eq__(self, other):
		return (isinstance(other, self.__class__) and self.subject == other.subject)

class Post(models.Model):
	topic = models.ForeignKey(Topic)
	user = models.ForeignKey(User)
	send_date = models.DateTimeField()
	content = models.TextField()

	def __unicode__(self):
		return str(self.send_date) + ' in topic "' + self.topic.__unicode__() + '" by ' + self.user.__unicode__()
