from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.

def index(request):
	return HttpResponse("Hello, world. You're at forum index.")

def topic(request, topic_id):
	return HttpResponse("You are looking at topic %s." % topic_id)

def user(request, user_id):
	return HttpResponse("You are looking at user %s." % user_id)

