#!/usr/bin/python
# -*- coding: utf8 -*-

from forum_app.models import User
from forum_app.models import Topic
from forum_app.models import Post
from django.utils import timezone
from django.db.utils import IntegrityError
import os
import re
import datetime
import xml.etree.ElementTree as ET
import time

def setup():
	os.system("./manage.py sqlclear forum_app | ./manage.py dbshell")
	os.system("./manage.py syncdb")


def loadData(inputFile):
	topics = []
	users = []
	posts = []
	tree = ET.parse(inputFile)
	root = tree.getroot()
	startTime = time.time()
	count = 0
	for entry in root:
		count += 1
		userInfo = entry.find("rule[@name='user-data']").text.strip()
		userData = parseUserInfo(userInfo)
		u = User( name = entry.find("rule[@name='user-login']").text.strip(),
			location = userData['location'], 
			registered = userData['registred'])

		t = Topic( subject = entry.find("rule[@name='thread-title']").text.strip()[7:])
		
		postInfo = entry.find("rule[@name='post-details']").text.strip()
		sendDate = parsePostInfo(postInfo)
		

		if (not u in users):
			users.append(u)
		else:
			u = users[users.index(u)]

		if (not t in topics):
			t.create_user = u
			t.create_date = sendDate
			topics.append(t)
		else:
			t = topics[topics.index(t)]
			if (t.create_date > sendDate):
				t.create_date = sendDate
				t.create_user = u

		p = Post( topic = t, user = u, 
			send_date = sendDate,
			content = entry.find("rule[@name='post-content']").text.strip())
		posts.append(p)

	endTime = time.time()
	print 'Data processing time (xml parsing): ' + str(endTime - startTime) + 's'
	startTime = time.time()

	User.objects.bulk_create(users)
	users = User.objects.all()

	for t in topics:
		t.create_user = users.get(name = t.create_user.name)

	Topic.objects.bulk_create(topics)
	topics = Topic.objects.all()

	for p in posts:
		p.user = users.get(name = p.user.name)
		p.topic = topics.get(subject = p.topic.subject)

	Post.objects.bulk_create(posts)
	endTime = time.time()
	print 'Data loading time (insert to DB): ' + str(endTime - startTime) + 's'


def run():
	setup()	
	loadData('../forum_data/tolkien.xml')

def parseUserInfo(userInfo):
	months = {'Sty' : 1, 'Lut' : 2, 'Mar' : 3, 'Kwi' : 4, 'Maj' : 5, 'Cze' : 6, 
		'Lip' : 7, 'Sie' : 8, 'Wrz' : 9, u'Paź' : 10, 'Lis' : 11, 'Gru' : 12}
	MO = re.search(u'Dołączył\(a\): (.*)', userInfo)
	if (MO != None):
		dateStr = MO.group(1).split(' ')
		registryDate = datetime.date(int(dateStr[2]), months[dateStr[1]], int(dateStr[0]))
	else:
		registryDate = None
	MO = re.search(u'Skąd: (.*)', userInfo)
	location = "" if MO == None else MO.group(1)
	return {'location' : location, 'registred' : registryDate}


def parsePostInfo(postInfo):
	dateMO = re.search(u'Wysłany: (\\d{2})-(\\d{2})-(\\d{4}) (\\d{2}):(\\d{2})', postInfo)
	try:
		sendDate = datetime.datetime(int(dateMO.group(3)), int(dateMO.group(2)), 
				int(dateMO.group(1)), int(dateMO.group(4)), int(dateMO.group(5)))
	except:
		postInfo = postInfo.replace(u'Wysłany: Wczoraj o', u'Wysłany: 09-04-2014')
		postInfo = postInfo.replace(u'Wysłany: Dzisiaj o', u'Wysłany: 10-04-2014')
		sendDate = parsePostInfo(postInfo)
	return sendDate
