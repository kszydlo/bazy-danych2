#!/usr/bin/python
# -*- coding: utf8 -*-

from forum_app.models import User
from forum_app.models import Topic
from forum_app.models import Post
from django.db.models import Avg
from django.db.models import Count
from django.db.models import F
import time
import re


def benchmark(f):
	def wrap_f():
		start = time.time()
		r = f()
		stop = time.time()
		return {'results' : r, 'time' : stop-start }
	wrap_f.__name__ = f.__name__
	return wrap_f


#number of topics created in 2013
@benchmark
def firstQuery():
	return Topic.objects.filter(create_date__year = 2013).filter(create_date__month = 5).count()


#most popular topic of May 2013
@benchmark
def secondQuery():
	results = Post.objects.filter(send_date__year = 2013).filter(send_date__month = 5).values('topic').annotate(total=Count('id')).order_by('-total')
	return Topic.objects.get(id=results[0]['topic'])


#average length of post
@benchmark
def thirdQuery():
	contentList = Post.objects.values('content')
	return reduce(lambda x, y: x + len(y['content']), contentList, 0)/len(contentList)


#most active user (posting in highest number of topics)
@benchmark
def forthQuery():
	users = User.objects.values('id').annotate(Count('post__topic', distinct = True)).order_by('-post__topic__count')
	return User.objects.get(id=users[0]['id'])


#most active commentator
@benchmark
def fifthQuery():
	users = Post.objects.exclude(user=F('topic__create_user')).values('user').annotate(Count('id')).order_by('-id__count')
	return User.objects.get(id=users[0]['user'])


#number of post containing word 'Frodo'
@benchmark
def sixthQuery():
	return Post.objects.filter(content__contains='Frodo').count()


#number of post written by users from city starting with 'K'
@benchmark
def seventhQuery():
	return Post.objects.filter(user__location__istartswith = 'k').count()


#35th most popular word in posts
@benchmark
def eighthQuery():
	contents = Post.objects.values('content')
	result = {}
	for c in contents:
		words = re.findall('\w+', c['content'], re.UNICODE)
		for w in words:
			if not w in result:
				result[w] = 1
			else:
				result[w] += 1
	return sorted(result, key=result.get, reverse=True)[35]


queries = [firstQuery, secondQuery, thirdQuery, forthQuery,
				fifthQuery, sixthQuery, seventhQuery, eighthQuery]


def test(repetitions = 10):
	results = { firstQuery : [], secondQuery : [], thirdQuery : [], forthQuery : [],
				fifthQuery : [], sixthQuery : [], seventhQuery : [], eighthQuery : [] }	

	#modify output file name here
	output = open('results/result_' + time.strftime('%Y-%m-%d_%H:%M:%S' , 
				time.localtime()) + '.txt', 'w')
	for i in xrange(0, repetitions):
		for query in queries:
			t = query()['time']
			results[query].append(t)
			output.write('{0:.6f}'.format(t) + '\t')
		output.write('\n')
	output.write('\n')
	for query in queries:
		output.write('{0:.6f}'.format(
			reduce(lambda x,y: x + y, results[query])/repetitions) + '\t')
	output.write('\n')
	output.close()


def printResults():
	for query in queries:
		r = query()
		print query.__name__ + ' executed in ' + str(r['time']) + 's'
		print 'result: ' + unicode(r['results']) + '\n'


def run(*args):
	if 'print' in args:
		printResults()
	if 'test' in args:
		if len(args)-1 == args.index('test'):
			test()
		else:
			test(int(args[args.index('test')+1]))

