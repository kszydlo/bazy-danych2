// if the database is empty on server start, create some sample data.
Meteor.startup(function () {
  if (Tematy.find().count() === 0) {
    var data = [
      {name: "Silmarilion",
       contents: [
         ["Czy ma ktoś streszczenie Silmariliona? Nie chce mi się czytać", "lama"],
         ["W piekle jest specjalny kocioł dla ciebie", "Thor"]
       ]
      },
      {name: "Pierścień Władzy",
       contents: [
         ["Kto wykuł perścienie elfów", "Zbyszek"],
         ["Same je wykuły", "Elladen"]
         ]
      },
      {name: "Universum Marvela - Spiderman",
       contents: [
         ["Wypowiedź 1", "osoba 1"],
         ["Wypowiedź 2", "osoba 2"]
       ]
      }
    ];

    var timestamp = (new Date()).getTime();
    for (var i = 0; i < data.length; i++) {
      var temat_id = Tematy.insert({name: data[i].name});
      for (var j = 0; j < data[i].contents.length; j++) {
        var info = data[i].contents[j];
        posty.insert({temat_id: temat_id,
                      text: info[0],
                      timestamp: timestamp,
                      autor: info.slice(1)});
        timestamp += 1; // ensure unique timestamp.
      }
    }
  }
});
