// Tematy -- {name: String}
Tematy = new Meteor.Collection("tematy");

// Publish complete set of tematy to all clients.
Meteor.publish('tematy', function () {
  return Tematy.find();
});


// posty -- {text: String,
//           done: Boolean,
//           autor: [String],
//           temat_id: String,
//           timestamp: Number}
posty = new Meteor.Collection("posty");

// Publish all items for requested list_id.
Meteor.publish('posty', function (temat_id) {
  check(temat_id, String);
  return posty.find({temat_id: temat_id});
});

