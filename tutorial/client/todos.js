// Client-side JavaScript, bundled and sent to client.

// Define Minimongo collections to match server/publish.js.
Tematy = new Meteor.Collection("tematy");
posty = new Meteor.Collection("posty");

// ID of currently selected list
Session.setDefault('temat_id', null);

// Name of currently selected tag for filtering
Session.setDefault('tag_filter', null);

// When adding tag to a post, ID of the post
Session.setDefault('editing_addtag', null);

// When editing a list name, ID of the list
Session.setDefault('editing_listname', null);

// When editing post text, ID of the post
Session.setDefault('editing_itemname', null);

// Subscribe to 'tematy' collection on startup.
// Select a list once data has arrived.
var tematyHandle = Meteor.subscribe('tematy', function () {
  if (!Session.get('temat_id')) {
    var list = Tematy.findOne({}, {sort: {name: 1}});
    if (list)
      Router.setList(list._id);
  }
});

var postyHandle = null;
// Always be subscribed to the posty for the selected list.
Deps.autorun(function () {
  var temat_id = Session.get('temat_id');
  if (temat_id)
    postyHandle = Meteor.subscribe('posty', temat_id);
  else
    postyHandle = null;
});


////////// Helpers for in-place editing //////////

// Returns an event map that handles the "escape" and "return" keys and
// "blur" events on a text input (given by selector) and interprets them
// as "ok" or "cancel".
var okCancelEvents = function (selector, callbacks) {
  var ok = callbacks.ok || function () {};
  var cancel = callbacks.cancel || function () {};

  var events = {};
  events['keyup '+selector+', keydown '+selector+', focusout '+selector] =
    function (evt) {
      if (evt.type === "keydown" && evt.which === 27) {
        // escape = cancel
        cancel.call(this, evt);

      } else if (evt.type === "keyup" && evt.which === 13 ||
                 evt.type === "focusout") {
        // blur/return/enter = ok/submit if non-empty
        var value = String(evt.target.value || "");
        if (value)
          ok.call(this, value, evt);
        else
          cancel.call(this, evt);
      }
    };

  return events;
};

var activateInput = function (input) {
  input.focus();
  input.select();
};

////////// Tematy //////////

Template.tematy.loading = function () {
  return !tematyHandle.ready();
};

Template.tematy.tematy = function () {
  return Tematy.find({}, {sort: {name: 1}});
};

Template.tematy.events({
  'mousedown .list': function (evt) { // select list
    Router.setList(this._id);
  },
  'click .list': function (evt) {
    // prevent clicks on <a> from refreshing the page.
    evt.preventDefault();
  },
  'dblclick .list': function (evt, tmpl) { // start editing list name
    Session.set('editing_listname', this._id);
    Deps.flush(); // force DOM redraw, so we can focus the edit field
    activateInput(tmpl.find("#list-name-input"));
  }
});

// Attach events to keydown, keyup, and blur on "New list" input box.
Template.tematy.events(okCancelEvents(
  '#new-list',
  {
    ok: function (text, evt) {
      var id = Tematy.insert({name: text});
      Router.setList(id);
      evt.target.value = "";
    }
  }));

Template.tematy.events(okCancelEvents(
  '#list-name-input',
  {
    ok: function (value) {
      Tematy.update(this._id, {$set: {name: value}});
      Session.set('editing_listname', null);
    },
    cancel: function () {
      Session.set('editing_listname', null);
    }
  }));

Template.tematy.selected = function () {
  return Session.equals('temat_id', this._id) ? 'selected' : '';
};

Template.tematy.name_class = function () {
  return this.name ? '' : 'empty';
};

Template.tematy.editing = function () {
  return Session.equals('editing_listname', this._id);
};

////////// posty //////////

Template.posty.loading = function () {
  return postyHandle && !postyHandle.ready();
};

Template.posty.any_list_selected = function () {
  return !Session.equals('temat_id', null);
};

Template.posty.events(okCancelEvents(   //dodanie
  '#new-post',
  {
    ok: function (text, evt) {
      var tag = Session.get('tag_filter');
      posty.insert({
        text: text,
        temat_id: Session.get('temat_id'),
        done: false,
        timestamp: (new Date()).getTime(),
        autor: 'Marek'
      });
      evt.target.value = '';
    }
  }));

Template.posty.posty = function () {
  // Determine which posty to display in main pane,
  // selected based on temat_id and tag_filter.

  var temat_id = Session.get('temat_id');
  if (!temat_id)
    return {};

  var sel = {temat_id: temat_id};
  var tag_filter = Session.get('tag_filter');
  if (tag_filter)
    sel.tags = tag_filter;

  return posty.find(sel, {sort: {timestamp: 1}});
};

Template.post_item.tag_objs = function () {
  var post_id = this._id;
  return _.map(this.tags || [], function (tag) {
    return {post_id: post_id, tag: tag};
  });
};

Template.post_item.done_class = function () {
  return this.done ? 'done' : '';
};

Template.post_item.editing = function () {
  return Session.equals('editing_itemname', this._id);
};

Template.post_item.adding_tag = function () {
  return Session.equals('editing_addtag', this._id);
};

Template.post_item.events({
  'click .check': function () {
    posty.update(this._id, {$set: {done: !this.done}});   //edycja
  },

  'click .destroy': function () {   //kasacja
    posty.remove(this._id);
  },

  'click .addtag': function (evt, tmpl) {
    Session.set('editing_addtag', this._id);
    Deps.flush(); // update DOM before focus
    activateInput(tmpl.find("#edittag-input"));
  },

  'dblclick .display .post-text': function (evt, tmpl) {
    Session.set('editing_itemname', this._id);
    Deps.flush(); // update DOM before focus
    activateInput(tmpl.find("#post-input"));
  },

  'click .remove': function (evt) {
    var tag = this.tag;
    var id = this.post_id;

    evt.target.parentNode.style.opacity = 0;
    // wait for CSS animation to finish
    Meteor.setTimeout(function () {
      posty.update({_id: id}, {$pull: {tags: tag}});
    }, 300);
  }
});

Template.post_item.events(okCancelEvents(
  '#post-input',
  {
    ok: function (value) {
      posty.update(this._id, {$set: {text: value}});
      Session.set('editing_itemname', null);
    },
    cancel: function () {
      Session.set('editing_itemname', null);
    }
  }));

Template.post_item.events(okCancelEvents(
  '#edittag-input',
  {
    ok: function (value) {
      posty.update(this._id, {$addToSet: {tags: value}});
      Session.set('editing_addtag', null);
    },
    cancel: function () {
      Session.set('editing_addtag', null);
    }
  }));

var postyRouter = Backbone.Router.extend({
  routes: {
    ":temat_id": "main"
  },
  main: function (temat_id) {
    var oldList = Session.get("temat_id");
    if (oldList !== temat_id) {
      Session.set("temat_id", temat_id);
      Session.set("tag_filter", null);
    }
  },
  setList: function (temat_id) {
    this.navigate(temat_id, true);
  }
});

Router = new postyRouter;

Meteor.startup(function () {
  Backbone.history.start({pushState: true});
});
